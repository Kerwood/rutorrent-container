# Simple ruTorrent
A lightweight and simple rutorrent container.

This image is build on [Simple rTorrent](https://gitlab.com/Kerwood/simple-rtorrent). Because ruTorrent needs to be on the same "host" or in the same container as rTorrent, I could not seperate rTorrent and ruTorrent in two different containers.


In the `assets` folder you vil find the configuration necessary for this container.
```
assets
├── init.sh
├── lighttpd.conf
├── rtorrent.rc
├── rtorrent-supervisord.ini
├── start-lighttpd.sh
└── start-rtorrent.sh
```

When the container starts, it will start the `supervisord` which til load the services in the `rtorrent-supervisord.ini`. The services are as follows.
 - `init` -> Runs `init.sh`
 - `rtorrent` -> Runs `start-rtorrent.sh`
 - `lighttpd` -> Runs `start-lighttpd.sh`

The `init` service will start when the container starts, and it will start the other services. If you want the container to do anything *before* rtorrent and lighttpd starts, the `init.sh` script is where you do it.

And finally, the `rtorrent.rc` file. We all know what that does.

Avaiable ports
- `80` - ruTorrent Webinterface.
- `5000` - rTorrent SCGI Port. Not necessary to expose unless you have an explicit reason for.
- `51001` - rTorrent traffic port. You open this port in your firewall/router.

The `PUID` and `PGID` are the user and group ID you want rTorrent to run as. Usually your own user/group ID on the host.

## Example
### Docker run command
```
docker run \
  --name ruTorrent \
  -p 8080:80 \
  -p 51001:51001 \
  -v /home/downloads:/downloads \
  -v /home/watch:/watch \
  -e PUID=1000 \
  -e PGID=1000 \
  -d registry.gitlab.com/kerwood/rutorrent-container:latest
```

### Docker Compose
```
version: '3.7'

networks:
  default:
    name: rtorrent

services:
  rtorrent:
    image: registry.gitlab.com/kerwood/rutorrent-container:latest
    container_name: rutorrent
    ports:
      - 8080:80 # ruTorrent Webinterface
      - 51001:51001 # rTorrent port.
    volumes:
      - /home/downloads:/downloads
      - /home/watch:/watch
    environment:
      - PUID=1000 # The UID of the rtorrent user
      - PGID=1000 # The group ID of the rtorrent user.
```