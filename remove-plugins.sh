#!/bin/bash

declare -a plugins

# This script will delete all plugins except those in below list.
# You can add or delete to fit your needs.
# You can find the names of the plugin folders here: https://github.com/Novik/ruTorrent/tree/master/plugins

plugins=(
  httprpc # This plugin is mandatory, do not remove.
  _noty2
  autotools
  cpuload
  data
  erasedata
  ratio
  extratio
  filedrop
  ipad
  lookat
  retrackers
  seedingtime
  show_peers_like_wtorrent
  theme
  throttle
  tracklabels
  check_port
)

removeDirectory () {
  path=$1
  basename=$(basename ${1})
  if [[ ! " ${plugins[@]} " =~ " ${basename} " ]]; then
    rm -rf $path
  fi
}

find ./plugins -mindepth 1 -maxdepth 1 -type d | while read folder
do
  removeDirectory $folder
done