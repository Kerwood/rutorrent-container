#!/bin/sh
set -e
NB_CORES=${BUILD_CORES-$(getconf _NPROCESSORS_CONF)}
apk -U upgrade
apk add --no-cache build-base git libtool automake autoconf tar xz binutils curl curl-dev cppunit-dev libressl-dev zlib-dev linux-headers ncurses-dev libxml2-dev

  # compile xmlrpc-c
cd /tmp
curl -L -O https://downloads.sourceforge.net/project/xmlrpc-c/Xmlrpc-c%20Super%20Stable/1.51.06/xmlrpc-c-1.51.06.tgz
tar zxvf xmlrpc-c-1.51.06.tgz
cd xmlrpc-c-1.51.06
./configure --enable-libxml2-backend --disable-cgi-server --disable-libwww-client --disable-wininet-client --disable-abyss-server
make -j ${NB_CORES}
make install
make -C tools -j ${NB_CORES}
make -C tools install

# compile libtorrent
cd /tmp
git clone https://github.com/rakshasa/libtorrent.git
cd /tmp/libtorrent
git checkout v0.13.8
./autogen.sh
./configure
make -j ${NB_CORES}
make install

# compile rtorrent
cd /tmp
git clone https://github.com/rakshasa/rtorrent.git
cd /tmp/rtorrent
git checkout v0.9.8
./autogen.sh
./configure --with-xmlrpc-c
make -j ${NB_CORES}
make install

ls -hl /usr/local/bin/rtorrent

# Strip that shit
strip -s /usr/local/bin/rtorrent
strip -s /usr/local/lib/libtorrent.so
strip -s /usr/local/bin/xmlrpc

ls -hl /usr/local/bin/rtorrent
