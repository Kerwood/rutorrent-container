#!/bin/sh
set -e

echo " ____        _                            _   "
echo "|  _ \ _   _| |_ ___  _ __ _ __ ___ _ __ | |_ "
echo "| |_) | | | | __/ _ \| '__| '__/ _ \ '_ \| __|"
echo "|  _ <| |_| | || (_) | |  | | |  __/ | | | |_ "
echo '|_| \_\\__,_|\__\___/|_|  |_|  \___|_| |_|\__|'
echo
echo "Powered by a bunch of small penguins.. @ Kerwood"
echo

function echo_log {
    TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
    echo "$TIMESTAMP --> $*"
}

##################################
#       Setting PUID/GUID        #
##################################

CURRENT_PUID=$(id -u rtorrent)
CURRENT_PGID=$(id -G rtorrent)

if [ ! -z ${PUID+x} ] && [ $PUID != $CURRENT_PUID ]; then
    echo_log "INFO Changing rtorrent PUID to ${PUID}"
    usermod -u $PUID rtorrent
    find /home/ -user $CURRENT_PUID -exec chown -h rtorrent {} \;
else
    echo_log "INFO PUID et set to $CURRENT_PUID"
fi

if [ ! -z ${PGID+x} ] && [ $PGID != $CURRENT_PGID ]; then
    echo_log "INFO Changing rtorrent PGID to ${PGID}"
    groupmod -g $PGID rtorrent
    find /home/ -group $CURRENT_PGID -exec chgrp -h rtorrent {} \;
else
    echo_log "INFO PGID et set to $CURRENT_PGID"
fi

##################################
#       Starting rTorrent        #
##################################

echo_log "INFO Starting rtorrent..."
supervisorctl start rtorrent

##################################
#        Starting lighttpd       #
##################################

echo_log "INFO Starting lighttpd.."
supervisorctl start lighttpd

